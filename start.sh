#!/usr/bin/env bash

cd ./dev/dns
docker-compose up -d

cd ../../x509/output
cp auth-service-https/* ../../../pki-auth/secrets/

cp ocsp-service/* ../../../pki-ocsp/secrets/
cp ocsp-service-https/* ../../../pki-ocsp/secrets/

cd ../../../pki-auth
docker-compose up -d

cd ../pki-ca
docker-compose up -d

