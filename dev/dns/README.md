# MegaTravel DNS

DNS server for local development of MegaTravel PKI.

## Usage

Start DNS server:
```bash
$ docker-compose up -d
```
DNS server will now be running on `172.16.0.1`.

Check if everything is working correctly:
```bash
$ host www.google.com 172.16.0.1
Using domain server:
Name: 172.16.0.1
Address: 172.16.0.1#53
Aliases:

www.google.com has address 172.217.19.100
www.google.com has IPv6 address 2a00:1450:400d:804::2004
```


Edit `/etc/resolv.conf` file and add our DNS server as first entry:
```bash
$ cat /etc/resolv.conf

nameserver 172.16.0.1
nameserver 192.168.1.1

```
>This file may be overridden and you may need to do this again.

Domain names for our services are preconfigured, but if you need to make some changes you can connect to *Webmin*
interface. Go to `https://ns.megatravel.com:10000/` and login with:
```text
username: root
password: password
```

From *Servers* menu select *BIND DNS Server* and then click on *<span>megatravel.com</span>* DNS zone under *Existing DNS Zones*.
Now select *Addresses* and there you can make your edits.