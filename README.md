# pki-tools

Common tools for MegaTravel PKI development.

## Installation

Commands described below assume that you are position at the root of this folder.

Create new virtual environment and activate it:

```bash
$ virtualenv venv
```

```bash
$ source venv/bin/activate
```

Install required dependencies:
```bash
$ pip install -r requirements.txt
```

Install project in editable mode:
```bash
$ pip install -e .
```

## Usage

To start all services at once run:
```bash
$ ./start.sh
```
> Note: you may first need to make this script executable (`chmod +x start.sh`)