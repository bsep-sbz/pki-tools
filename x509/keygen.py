import os

from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives.asymmetric import ec
from cryptography.hazmat.primitives import serialization


def generate():
    private_key = ec.generate_private_key(ec.SECP256R1(), default_backend())
    public_key = private_key.public_key()
    return public_key, private_key


def serialize(public_key=None, private_key=None):
    if not public_key and not private_key:
        raise ValueError('Serialization failed. No keys passed.')

    serialized_private = None
    if private_key:
        serialized_private = private_key.private_bytes(
            encoding=serialization.Encoding.PEM,
            format=serialization.PrivateFormat.PKCS8,
            encryption_algorithm=serialization.NoEncryption()
        )

    serialized_public = None
    if public_key:
        serialized_public = public_key.public_bytes(
            encoding=serialization.Encoding.PEM,
            format=serialization.PublicFormat.SubjectPublicKeyInfo,
        )

    return serialized_public, serialized_private


def persist(file_name, public_key, private_key, output_dir='.'):
    pub_path = os.path.join(output_dir, f'{file_name}-pub.key')
    with open(pub_path, 'wb') as f:
        f.write(public_key)

    pri_path = os.path.join(output_dir, f'{file_name}.key')
    with open(pri_path, 'wb') as f:
        f.write(private_key)


if __name__ == '__main__':
    public_key, private_key = generate()
    serialized_public, serialized_private = serialize(public_key, private_key)
    persist(
        input('Key file name: '),
        serialized_public,
        serialized_private,
        os.path.join('.', 'output')
    )
